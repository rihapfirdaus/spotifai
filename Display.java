package spotifai;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;


/**
 * Class Display berisi method-method yang berkaitan
 * dengan UI pada program
 */
public class Display {
    private Scanner scan = new Scanner(System.in);

    Additional additional = new Additional();

    /**
     * Method untuk menampilkan header program
     */
    public void headerSection(){
        additional.separator();
        additional.textToCenter("SPOTIFAI");
        additional.separator();
    }

    /**
     * Method untuk menampilkan opsi masuk
     */
    void entryOption(){
        int userInput;

        additional.textToCenter("already have an account?");
        additional.textToCenter("1. SignIn 2. SignUp");

        while (true) {
            System.out.print("> (1/2): ");

            try {
                userInput = scan.nextInt();
                scan.nextLine();

                if (userInput == 1) {
                    additional.clearScreen();

                    signInSection();
                    break;
                }else if (userInput == 2){
                    additional.clearScreen();

                    signUpSection();
                    break;
                }else{
                    additional.inputMismatch();  
                }
            } catch (InputMismatchException e) {
                additional.textToCenter("Input mismatch. Please input a valid number.");
                additional.clearFromLine(2000, 2);
                scan.next(); 
                continue;
            }
        }
    }
    
    void signUpSection(){
        Data data = new Data();

        additional.textToCenter("SIGNUP");
        additional.textToCenter("please set up your account");
        System.out.println("");
        System.out.print(" username: ");
        String userName = scan.nextLine();
        System.out.print(" name    : ");
        String nama = scan.nextLine();
        System.out.print(" password: ");
        String password = scan.nextLine();
        System.out.println("");

        data.setUser(userName, nama, password);
    }
    void signInSection(){
        Data data = new Data();
        int count = 0;
        int userInput;

        additional.textToCenter("SIGNIN");
        if (data.getUsername() == null) {
            additional.textToCenter("you don't have an account!!");
            additional.textToCenter("please create one first");
            System.out.println("1. Go to sign up page");
            System.out.println("2. Exit the application");

            while(true) {
                System.out.print("> (1/2): ");

                try {
                    userInput = scan.nextInt();
                    scan.nextLine();

                    if (userInput == 1) {
                        additional.clearScreen();
                        this.signUpSection();
                        break;
                    } else if (userInput == 2){
                        additional.exitMessage();
                        System.exit(0);
                    } else {
                        additional.inputMismatch();
                    }
                } catch (InputMismatchException e) {
                    additional.textToCenter("Input mismatch. Please input a valid number.");
                    additional.clearFromLine(2000, 2);
                    scan.next(); 
                    continue;
                }
            }
        } else {
            additional.textToCenter("please log in with your account");
            while(true && count < 3) {
                System.out.println("");
                System.out.print(" username: ");
                String username = scan.nextLine();
                System.out.print(" password: ");
                String password = scan.nextLine();
                System.out.println("");
                
                if (data.getUsername().equals(username) && data.getPassword().equals(password)) {
                    return;
                }else{
                    count++;
                    additional.textToCenter("incorrect username or password");
                    additional.textToCenter("please try again..");
                    additional.clearFromLine(2000, 6);
                }
            }
            additional.textToCenter("You're wrong three times, please try again later..");
            additional.textToCenter("Enter any key to exit");
            scan.nextLine();
            additional.exitMessage();
            System.exit(0);
        }

    }

    /**
     * Method untuk menampilkan konten yang saat ini diputar
     * dan kondisi player saat ini. 
     */
    void recentPlaySection(){
        Data data = new Data();
        Features.MusicPlayer playerMusic = new Features().new MusicPlayer();
        Features.PodcastPlayer playerPodcast = new Features().new PodcastPlayer();

        ArrayList<DataObj.History> history = data.getHistory();

        additional.separator();
        if (history.isEmpty()) {
            additional.textToCenter("you haven't heard anything yet");
        }else{
            String typeLast = history.get(history.size()-1).getTypeLast();
            int indexLast = history.get(history.size()-1).getIndexLast();
            if (typeLast.equals("music")) {
                playerMusic.recent(indexLast);
            }else if (typeLast.equals("podcast")){
                playerPodcast.recent(indexLast);
            }
        }
        additional.separator();
    }

    /**
     * Method untuk menampilkan list data konten yang diminimalkan. 
     */
    void listContentSection(){
        Data data = new Data();
        ArrayList<DataObj.Music> music = data.getMusic();
        ArrayList<DataObj.Podcast> podcast = data.getPodcast();

        int maxIndex;

        System.out.println("\n\033[1mList of Music\033[0m");

        maxIndex = music.size();
        if (music.isEmpty()) {
            System.out.println("go to developer menu to add a song");
        } else { 
            if(music.size() > 3){
                maxIndex = 3;
            }
            
            for(int i = 0; i < maxIndex; i++){
                System.out.println(i + 1 + ". " + music.get(i).getTitle() + " - " + music.get(i).getArtist());
            }
        }

        System.out.println("\n\033[1mList of Podcast\033[0m");

        maxIndex = podcast.size();
        if (podcast.isEmpty()){
            System.out.println("go to developer menu to add a podcast");
        } else {
            if (podcast.size() > 3){
                maxIndex = 3;
            }
            
            for(int i = 0; i < maxIndex; i++){
                System.out.println(i + 1 + ". " + podcast.get(i).getTitle() + " - " + podcast.get(i).getEpsCount() + " Episode");
            }
        }
    }

    /**
     * Method untuk menampilkan list data history yang
     * diminimalkan. 
     */
    void listHistorySection(){
        ArrayList<DataObj.History> history= new Data().getHistory();
        ArrayList<DataObj.Music> music= new Data().getMusic();
        ArrayList<DataObj.Podcast> podcast= new Data().getPodcast();

        int startIndex;

        System.out.println("\n\033[1mHistory\033[0m");

        startIndex = 0;
        if (history.isEmpty()) {
            System.out.println("you haven't heard anything yet");
        }else{
            if (history.size() > 3){
                startIndex = history.size()-3;
            }

            for(int i = startIndex; i < history.size(); i++){
                String typeLast = history.get(i).getTypeLast();
                int indexLast = history.get(i).getIndexLast();
                
                if (typeLast.equals("music")) {
                    System.out.println(i + 1 + ". " + typeLast + " " + music.get(indexLast).getTitle() + " - " + music.get(indexLast).getArtist());
                } else if (typeLast.equals("podcast")) {
                    System.out.println(i + 1 + ". " + typeLast + " " + podcast.get(indexLast).getTitle());
                }
            }
        }
    }

    /**
     * Method  yang digunakan untuk menampilkan seluruh konten
     * yang tersedia berdasarkan tipe
     * @param type tipe konten (music/podcast/history)
     */
    void listAllContent(String type){
        Menu menu = new Menu();
        ArrayList<DataObj.History> history= new Data().getHistory();
        ArrayList<DataObj.Music> music= new Data().getMusic();
        ArrayList<DataObj.Podcast> podcast= new Data().getPodcast();

        additional.clearScreen();
        if(type.equals("history")){
            additional.textToCenter("List History");
            System.out.println("");

            if (history.isEmpty()) {
                additional.textToCenter("you haven't heard anything yet");
                additional.delay(2000);
            } else {
                for (int i = 0; i < history.size(); i++) {
                    String typeLast = history.get(i).getTypeLast();
                    int indexLast = history.get(i).getIndexLast();
                    
                    if (typeLast.equals("music")) {
                        System.out.println(i + 1 + ". " + typeLast + " " + music.get(indexLast).getTitle() + " - " + music.get(indexLast).getArtist());
                    } else if (typeLast.equals("podcast")) {
                        System.out.println(i + 1 + ". " + typeLast + " " + podcast.get(indexLast).getTitle());
                    }
                }
                System.out.println("");
                this.recentPlaySection();
                while (true) {
                    System.out.print("> (input 0 to return home): ");
                    try {
                        if (scan.nextInt() == 0) {
                            break;
                        }else{
                            additional.inputMismatch();
                        }
                    } catch (InputMismatchException e) {
                        additional.textToCenter("Input mismatch. Please input a valid number.");
                        additional.clearFromLine(2000, 2);
                        scan.next(); 
                        continue;
                    }
                }
                
            }
        } else if (type.equals("music")){
            additional.textToCenter("List Music");
            System.out.println("");
            
            if (music.isEmpty()){
                additional.textToCenter("go to developer menu to add a music");
                additional.delay(2000);
            } else {
                for (int i = 0; i < music.size(); i++) {
                    System.out.println(i + 1 + ". " + music.get(i).getTitle() + " - " + music.get(i).getArtist());
                }
                System.out.println("");
                this.recentPlaySection();
                menu.optionPlayer();
            }
        } else if (type.equals("podcast")){
            additional.textToCenter("List Podcast");
            System.out.println("");

            if (podcast.isEmpty()){
                additional.textToCenter("go to developer menu to add a podcast");
                additional.delay(2000);
            } else {
                for (int i = 0; i < podcast.size(); i++) {
                    System.out.println(i + 1 + ". " + podcast.get(i).getTitle() + " - " + podcast.get(i).getEpsCount() + " Episode");
                }
                System.out.println("");
                this.recentPlaySection();
                menu.optionPlayer();
            }
        } 
    }
}
