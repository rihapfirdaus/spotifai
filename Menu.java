package spotifai;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Class Menu berisi menu-menu yang ada dalam program. 
 */
public class Menu {
    private Scanner scan = new Scanner(System.in);

    Features features = new Features();
    Additional additional = new Additional();
    Display displays = new Display();
    Data data = new Data();

    /**
     * Method untuk menampilkan Menu startup, saat program pertama kali dimulai
     */
    public void start(){
        additional.clearScreen();
        displays.entryOption();
        additional.message();
        this.mainMenu();
    }
    
    /**
     * Method untuk menampilkan Menu Utama
     */
    public void mainMenu(){
        additional.clearScreen();
        additional.getGreetings();
        displays.listHistorySection();
        displays.listContentSection();
        displays.recentPlaySection();
        this.optionPlayer();
    }

    /**
     * Method untuk menampilkan opsi pemutar. 
     * Membaca kondisi apakah pemutar sebelumnya
     * berada pada play/pause.
     */
    public void optionPlayer(){
        Features.MusicPlayer playerMusic = new Features().new MusicPlayer();
        Features.PodcastPlayer playerPodcast = new Features().new PodcastPlayer();
        
        ArrayList<DataObj.History> history = new Data().getHistory();
        String condition = data.getCondition();
        int userInput;

        if(condition.equals("pause")){
            additional.textToCenter("1. prev 2. play 3. next");
        } else if (condition.equals("play")) {
            additional.textToCenter("1. prev 2. pause 3. next");
        }
        additional.textToCenter("4. more");

        while (true) {
            System.out.print("> (1-4): ");
            try {
                userInput = scan.nextInt();
                switch (userInput) {
                    case 1:
                        if (history.isEmpty()) {
                            additional.textToCenter("please listen to something first");
                            additional.clearFromLine(2000, 2);
                            continue;
                        } else {
                            String typeLast = history.get(history.size()-1).getTypeLast();
                            int indexLast = history.get(history.size()-1).getIndexLast();
                            
                            if(typeLast.equals("music")){
                                playerMusic.prev(indexLast);
                            } else if (typeLast.equals("podcast")){
                                playerPodcast.prev(indexLast);
                            }
                            break;
                        }
                    case 2:
                        if (condition.equals("pause")){
                            if (history.isEmpty()) {
                                additional.clearFromLine(500, 3);
                                additional.textToCenter("what you want to play");
                                additional.textToCenter("1. music 2. podcast");
                                while (true) {
                                    System.out.print("> (1/2): ");
                                    int play = scan.nextInt();
                                    if (play == 1) {
                                        System.out.print("> input index of music: ");
                                        playerMusic.play(scan.nextInt()-1);
                                        break;
                                    } else if (play == 2){
                                        System.out.print("> input index of podcast: ");
                                        playerPodcast.play(scan.nextInt()-1);
                                        break;
                                    } else {
                                        additional.inputMismatch();
                                    }
                                }
                            } else {
                                additional.clearFromLine(500, 3);
                                additional.textToCenter("1. resume 2. by index");
                                System.out.print("> (1/2): ");
                                int play = scan.nextInt();
                                if (play == 1) {
                                    String typeLast = history.get(history.size()-1).getTypeLast();
                                    int indexLast = history.get(history.size()-1).getIndexLast();
    
                                    if (typeLast.equals("music")) {
                                        playerMusic.play(indexLast);
                                    } else if (typeLast.equals("podcast")) {
                                        playerPodcast.play(indexLast);
                                    }
                                    break;
                                } else if (play == 2) {
                                    additional.clearFromLine(500, 2);
                                    additional.textToCenter("what you want to play");
                                    additional.textToCenter("1. music 2. podcast");
                                    while (true) {
                                        System.out.print("> (1/2): ");
                                        play = scan.nextInt();
                                        if (play == 1) {
                                            System.out.print("> input index of music: ");
                                            playerMusic.play(scan.nextInt()-1);
                                            break;
                                        } else if (play == 2){
                                            System.out.print("> input index of podcast: ");
                                            playerPodcast.play(scan.nextInt()-1);
                                            break;
                                        } else {
                                            additional.inputMismatch();
                                        }
                                    }
                                } else {
                                    additional.inputMismatch();
                                    break;
                                }
                            }
                        } else if (condition.equals("play")){
                            if (history.isEmpty()) {
                                additional.textToCenter("please listen to something first");
                                additional.clearFromLine(2000, 2);
                                continue;
                            } else {
                                String typeLast = history.get(history.size()-1).getTypeLast();
                                int indexLast = history.get(history.size()-1).getIndexLast();

                                if (typeLast.equals("music")) {
                                    playerMusic.pause(indexLast);
                                } else if (typeLast.equals("podcast")) {
                                    playerPodcast.pause(indexLast);
                                }
                            }
                        }
                        break;
                    case 3:
                        if (history.isEmpty()) {
                            additional.textToCenter("please listen to something first");
                            additional.clearFromLine(2000, 2);
                            continue;
                        } else {
                            String typeLast = history.get(history.size()-1).getTypeLast();
                            int indexLast = history.get(history.size()-1).getIndexLast();
                            
                            if(typeLast.equals("music")){
                                playerMusic.next(indexLast);
                            } else if (typeLast.equals("podcast")){
                                playerPodcast.next(indexLast);
                            }
                            break;
                        }
                    case 4:
                        this.optionMore();
                        break;
                
                    default:
                        additional.inputMismatch();
                        continue;
                }
            break;
            } catch (InputMismatchException e) {
                additional.textToCenter("Input mismatch. Please input a valid number.");
                additional.clearFromLine(2000, 2);
                scan.next(); 
                continue;
            }
        }
        this.mainMenu();
    }

    /**
     * Method untuk menampilkan opsi tambahan
     */
    public void optionMore(){
        int userInput;
        System.out.print("\033[3A\033[J"); 
        additional.textToCenter("4. other list music   7. go to developer menu");
        additional.textToCenter("5. other list podcast 8. go to setting       ");
        additional.textToCenter("6. see your history   9. exit program        ");
        additional.textToCenter("0. back");
        
        while(true){
            System.out.print("> (0/4-9): ");
            try {
                userInput = scan.nextInt();
                switch (userInput) {
                    case 4:
                        displays.listAllContent("music");
                        break;
                    case 5:
                        displays.listAllContent("podcast");
                        break;
                    case 6:
                        displays.listAllContent("history");
                        break;
                    case 7:
                        this.devMenu();
                        break;
                    case 8:
                        additional.textToCenter("coming soon..");
                        additional.delay(1500);
                        break;
                    case 9:
                        additional.exitMessage();
                        System.exit(0);
                    case 0:
                        this.mainMenu();
                        break;
                    default:
                        additional.inputMismatch();
                        continue;
                }
                break;
            } catch (InputMismatchException e) {
                additional.textToCenter("Input mismatch. Please input a valid number.");
                additional.clearFromLine(2000, 2);
                scan.next(); 
                continue;
            }

        }
    }

    /**
     * Method untuk menampilkan menu saat ingin menambah konten.
     */
    public void devMenu(){
        additional.clearScreen();
        additional.textToCenter("everyone can be a developer");
        while (true) {

            additional.clearScreen();

            additional.textToCenter("what you want to add?");
            additional.textToCenter("1. music 2. podcast");
            additional.textToCenter("3. update from system 0. back to home");

            System.out.print("> ");
            int userInput;
            try {
                userInput = scan.nextInt();
                if(userInput == 1){
                    additional.clearScreen();
                    additional.textToCenter("Add Music");
                    data.setMusic();
                    break;
                }else if(userInput == 2){
                    additional.clearScreen();
                    additional.textToCenter("Add Podcast");
                    data.setPodcast();
                    break;
                } else if(userInput == 3){
                    additional.clearScreen();
                    data.updateContent();
                    additional.delay(1500);
                    this.mainMenu();
                    break;
                } else if(userInput == 0){
                    this.mainMenu();
                    break;
                }else{
                    additional.inputMismatch();  
                }
    
            } catch (InputMismatchException e) {
                additional.textToCenter("Input mismatch. Please input a valid number.");
                additional.clearFromLine(2000, 2);
                scan.next(); 
                continue;
            }
        }
    }
}
