package spotifai;

import java.util.ArrayList;

/**
 * Class Features nested class fitur-fitur yang terdapat pada program. 
 */
public class Features {
    Additional additional = new Additional();

    /**
     * Abstract class yang merepresentasikan pemutar konten.
     * Subclass harus mengimplementasikan perilaku spesifik untuk setiap jenis pemutar.
     */
    abstract class Player{

        /**
         * Method untuk menampilkan file yang baru diakses serta
         * kondisi antara play dan pause. 
         * @param index index dari file terbaru
         */
        public abstract void recent(int index);

        /**
         * Method untuk menampilkan message play jika index yang
         * diminta tersedia dan message keterangan jika tidak
         * @param index index dari file yang akan di play
         */
        public abstract void play(int index);
        
        /**
         * method untuk menampilkan message pause
         * @param index
         */
        public abstract void pause(int index);
        
        public abstract void next(int index);
        public abstract void prev(int index);
    }

    /**
     * MusicPlayer merupakan subclass dari Player. 
     * Class ini mendeskripsikan fitur-fitur yang spesifik mengenai pemutar musik
     */
    class MusicPlayer extends Player{
        ArrayList<DataObj.History> history = new Data().getHistory();
        ArrayList<DataObj.Music> music = new Data().getMusic();
        Data data = new Data();

        @Override
        public void recent(int index){
            String condition = data.getCondition();
            String title = music.get(index).getTitle();

            if (condition.equals("play")) {
                additional.textToCenter("[playing] " + title);
            } else if (condition.equals("pause")){
                additional.textToCenter(title + " [paused]");
            }
        }
        @Override
        public void play(int index){
            String title = music.get(index).getTitle();

            if (index > music.size()-1) {
                additional.textToCenter("music unavailable");
                additional.delay(1500);
            } else {
                additional.textToCenter("playing " + title);
                additional.delay(1500);
                data.setCondition(1);
                data.setHistory(1, index);
            }
        }
        @Override
        public void pause(int index){
            String title = music.get(index).getTitle();

            additional.textToCenter(title + " paused");
            additional.delay(1500);
            data.setCondition(2);
        }

        /**
         * Method untuk berpindah ke index music selanjutnya, jika
         * sudah tidak tersedia index akan kembali ke 0. 
        * @param index index file yang sedang diputar.
         */
        @Override
        public void next(int index){
            int recentIndex;

            if (index == music.size()-1) {
                recentIndex = 0;
            } else {
                recentIndex = index + 1;
            }

            additional.textToCenter("playing next music");
            additional.delay(1500);
            this.play(recentIndex);
        }

        /**
         * Method untuk berpindah ke index music sebelumnya, jika
         * sudah tidak tersedia index akan kembali ke index akhir. 
        * @param index index file yang sedang diputar.
         */
        @Override
        public void prev(int index){
            int recentIndex;

            if (index == 0) {
                recentIndex = music.size()-1;
            } else {
                recentIndex = index - 1;
            }

            additional.textToCenter("playing previous music");
            additional.delay(1500);
            this.play(recentIndex);
        }
    }

    class PodcastPlayer extends Player{
        ArrayList<DataObj.History> history = new Data().getHistory();
        ArrayList<DataObj.Podcast> podcast = new Data().getPodcast();
        Data data = new Data();

        @Override
        public void recent(int index){
            String condition = data.getCondition();
            String tittle = podcast.get(index).getTitle();
            int episode = podcast.get(index).getEpsLast();

            if (condition.equals("play")) {
                additional.textToCenter("[playing] " + tittle + " episode " + episode);
            } else if (condition.equals("pause")){
                additional.textToCenter(tittle + " episode " + episode + " [paused]");
            }
        }
        @Override
        public void play(int index){
            String tittle = podcast.get(index).getTitle();
            int episode = podcast.get(index).getEpsLast();

            if (index > podcast.size()-1) {
                additional.textToCenter("podcast unavailable");
                additional.delay(1500);
            } else {
                additional.textToCenter("playing " + tittle + " episode " + episode);
                additional.delay(1500);
                data.setCondition(1);
                data.setHistory(2, index);
            }
        }
        @Override
        public void pause(int index){
            String tittle = podcast.get(index).getTitle();
            int episode = podcast.get(index).getEpsLast();

            additional.textToCenter(tittle + " episode " + episode + " paused");
            additional.delay(1500);
            data.setCondition(2);
        }

        /**
         * Method untuk memutar episode selanjutnya. Jika
         * sudah mencapai akhir episode podcast selanjutnya
         * akan diputar.
         * @param index index file yang sedang diputar.
         */
        @Override
        public void next(int index){
            int recentIndex;
            int lastEpisode = podcast.get(index).getEpsLast();
            int epsCount = podcast.get(index).getEpsCount();

            if (lastEpisode >= epsCount) {
                recentIndex = index + 1;
                if (index == podcast.size()-1) {
                    recentIndex = 0;
                }
                additional.textToCenter("playing next podcast");
                additional.delay(1500);
            } else {
                recentIndex = index;
                data.updateEpsLastPodcast(recentIndex, lastEpisode + 1);
                additional.textToCenter("playing next episode of podcast");
                additional.delay(1500);
            }

            this.play(recentIndex);
        }

        /**
         * Method untuk memutar episode sebelumnya. Jika
         * sudah mencapai awal episode podcast sebelumnya
         * akan diputar.
         * @param index index file yang sedang diputar.
         */
        @Override
        public void prev(int index){
            int recentIndex;
            int lastEpisode = podcast.get(index).getEpsLast();

            if (lastEpisode <= 1) {
                recentIndex = index - 1;
                if (index == 0) {
                    recentIndex = podcast.size()-1;
                }
                additional.textToCenter("playing previous podcast");
                additional.delay(1500);
            } else {
                recentIndex = index;
                data.updateEpsLastPodcast(recentIndex, lastEpisode-1);
                additional.textToCenter("playing previous episode of podcast");
                additional.delay(1500);
            }
            this.play(recentIndex);
        }
    }
}
