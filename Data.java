package spotifai;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.UUID;

/**
 * beberapa class pada DataObj mengimplementasikan Serializable
 * agar ArrayList Object yang dimilikinya bisa disimpan
 * dalam file dat untuk digunakan pada proses selanjutnya
 *  */ 

/**
 * class DataObj merupakan nested class berisi kumpulan class
 * object yang akan dibuat menjadi arrayList
 */
class DataObj implements Serializable{
    /** 
     * class object content
     * */
    class Content implements Serializable{
        private final UUID id = UUID.randomUUID();
        private String title;
        private String artist;
        private String genre;

        public void setTitle(String title){
            this.title = title;
        }
        public void setArtist(String artist){
            this.artist = artist;
        }
        public void setGenre(String genre){
            this.genre = genre;
        }
        
        public UUID getId(){
            return this.id;
        }
        public String getTitle(){
            return this.title;
        }
        public String getArtist(){
            return this.artist;
        }
        public String getGenre(){
            return this.genre;
        }
    }

    /** 
     * class object music
     * */
    class Music extends Content{
        private String album;
        private boolean isCollection;
        
        public void setAlbum(String album){
            this.album = album;
        }
        public void setCollection(boolean isCollection){
            this.isCollection = isCollection;
        }

        public String getAlbum(){
            return this.album;
        }
        public boolean getCollection(){
            return this.isCollection;
        }
    }

    /** 
     * class object podcast
     * */
    class Podcast extends Content{
        private int epsCount;
        private int epsLast;

        public void setEpsCount(int epsCount){
            this.epsCount = epsCount;
        }
        public void setEpsLast(int epsLast){
            this.epsLast = epsLast;
        }

        public int getEpsCount(){
            return this.epsCount;
        }
        public int getEpsLast(){
            return this.epsLast;
        }
    }

    /** 
     * class object history
     * */
    class History implements Serializable{
        private final UUID id = UUID.randomUUID();
        private String typeLast;
        private int indexLast;

        public void setTypeLast(int type){
            if (type == 1) {
                this.typeLast = "music";
            } else if (type == 2){
                this.typeLast = "podcast";
            }
        }
        public void setIndexLast(int index){
            this.indexLast = index;
        }

        public UUID getId(){
            return this.id;
        }
        public String getTypeLast(){
            return this.typeLast;
        }
        public int getIndexLast(){
            return this.indexLast;
        }
    }
}

/**
 * Class Data berisi method-method untuk menginisialisasi
 * object arraylist, menyimpan data ke database, dan membaca
 * data dari database. 
 */
@SuppressWarnings("unchecked")
class Data{
    Additional additional = new Additional();

    private Scanner scan = new Scanner(System.in);
    private ArrayList<DataObj.Music> music = new ArrayList<DataObj.Music>();
    private ArrayList<DataObj.Podcast> podcast = new ArrayList<DataObj.Podcast>();
    private ArrayList<DataObj.History> history = new ArrayList<DataObj.History>();
    
    private String username;    
    private String name;
    private String password;
    private String lastCondition = "pause";

    public void setUser(String username, String name, String password){
        this.username = username;
        this.name = name;
        this.password = password;
        this.writeUser();
    }

    /**
     * Method untuk menginisialisasi data musik dari sisi pengguna.
     */
    public void setMusic(){
        Menu menu = new Menu();
        DataObj.Music musicObj = new DataObj().new Music();
        
        String validation;

        this.readMusic();

        do{
            System.out.print("Tittle  : ");
            musicObj.setTitle(scan.nextLine());
            System.out.print("Artist  : ");
            musicObj.setArtist(scan.nextLine());
            System.out.print("Genre   : ");
            musicObj.setGenre(scan.nextLine());
            System.out.print("Album   : ");
            musicObj.setAlbum(scan.nextLine());
            
            musicObj.setCollection(false);
            
            while(true){
                additional.textToCenter("Is the data correct?");
                System.out.print("> (yes/no/cancel): ");
                validation = scan.nextLine();

                if (validation.equalsIgnoreCase("yes")) {
                    this.music.add(musicObj);
                    this.writeMusic();
                    additional.textToCenter("your music has been added");
                    additional.delay(2000);
                    menu.mainMenu();
                    break;
                } else if(validation.equalsIgnoreCase("no")){
                    additional.textToCenter("please input data correctly");
                    additional.clearFromLine(2000, 7);
                    break;
                } else if(validation.equalsIgnoreCase("cancel")){
                    additional.textToCenter("new music cancelled");
                    additional.delay(2000);
                    menu.mainMenu();
                    break;
                } else {
                    additional.textToCenter("wrong input, please reenter");
                    additional.clearFromLine(1500, 3);
                }
            }
        }while(validation.equals("no"));
    }

    /**
     * Overload dari method setMusic, untuk menginisialisasi data
     * music dari sisi programmer.
     * @param title judul musik.
     * @param Artist nama penyanyi.
     * @param genre genre musik.
     * @param album album musik.
     */
    public void setMusic(String title, String Artist, String genre, String album){
        DataObj.Music musicObj = new DataObj().new Music();

        this.readMusic();

        musicObj.setTitle(title);
        musicObj.setArtist(Artist);
        musicObj.setGenre(genre);
        musicObj.setAlbum(album);
        musicObj.setCollection(false);

        music.add(musicObj);
        this.writeMusic();
    }

    /**
     * Method untuk menginisialisasi data podcast dari sisi pengguna. 
     */
    public void setPodcast(){
        Menu menu = new Menu();
        DataObj.Podcast podcastObj = new DataObj().new Podcast();
        
        String validation;

        this.readPodcast();

        do{
            System.out.print("Tittle  : ");
            podcastObj.setTitle(scan.nextLine());
            System.out.print("Artist  : ");
            podcastObj.setArtist(scan.nextLine());
            System.out.print("Genre   : ");
            podcastObj.setGenre(scan.nextLine());
            System.out.print("Episode : ");
            podcastObj.setEpsCount(scan.nextInt());
            scan.nextLine();
            
            podcastObj.setEpsLast(1);
            
            while(true){
                additional.textToCenter("Is the data correct?");
                System.out.print("> (yes/no/cancel): ");
                validation = scan.nextLine();

                if (validation.equalsIgnoreCase("yes")) {
                    this.podcast.add(podcastObj);
                    this.writePodcast();
                    additional.textToCenter("your podcast has been added");
                    additional.delay(2000);
                    menu.mainMenu();
                    break;
                } else if(validation.equalsIgnoreCase("no")){
                    additional.textToCenter("please input data correctly");
                    additional.clearFromLine(2000, 7);
                    break;
                } else if(validation.equalsIgnoreCase("cancel")){
                    additional.textToCenter("new podcast cancelled");
                    additional.delay(2000);
                    menu.mainMenu();
                    break;
                } else {
                    additional.textToCenter("wrong input, please reenter");
                    additional.clearFromLine(1500, 3);
                }
            }
        }while(validation.equals("no"));
    }

    /**
     * Overload dari method setPodcast, untuk menginisialisasi data
     * podcast dari sisi programmer. 
     * @param title judul podcast. 
     * @param artist nama host podcast.
     * @param genre genre podcast. 
     * @param episode jumlah episode podcast.
     */
    public void setPodcast(String title, String artist, String genre, int episode){
        DataObj.Podcast podcastObj = new DataObj().new Podcast();

        this.readPodcast();

        podcastObj.setTitle(title);
        podcastObj.setArtist(artist);
        podcastObj.setGenre(genre);
        podcastObj.setEpsCount(episode);
        podcastObj.setEpsLast(1);

        podcast.add(podcastObj);
        this.writePodcast();
    }

    /**
     * Method untuk menginisilisasi data history.
     * @param type tipe konten (1: music/2: podcast).
     * @param index index dari konten.
     */
    public void setHistory(int type, int index){
        DataObj.History historyObj = new DataObj().new History();

        this.readHistory();

        historyObj.setTypeLast(type);
        historyObj.setIndexLast(index);

        history.add(historyObj);
        this.writeHistory();
    }

    /**
     * Method untuk menginisialisasi data dari player
     * yang sedang bekerja
     * @param condition tipe kondisi (1: pause/2: play)
     */
    public void setCondition(int condition){
        if(condition == 1){
            this.lastCondition = "play";
        }else if(condition == 2){
            this.lastCondition = "pause";
        }
        this.writeCondition();
    }

    /**
     * Method untuk mengupdate episode terakhir podcast.
     * @param index index podcast yang akan diupdate
     * @param epsLast nilai dari episode akhir
     */
    public void updateEpsLastPodcast(int index, int epsLast){
        DataObj.Podcast podcastObj = new DataObj().new Podcast();

        this.readPodcast();

        podcastObj.setTitle(this.podcast.get(index).getTitle());
        podcastObj.setArtist(this.podcast.get(index).getArtist());
        podcastObj.setGenre(this.podcast.get(index).getGenre());
        podcastObj.setEpsCount(this.podcast.get(index).getEpsCount());
        podcastObj.setEpsLast(epsLast);

        podcast.set(index, podcastObj);
        this.writePodcast();
    }

    public String getUsername(){
        this.readUser();
        return this.username;
    }
    public String getName(){
        this.readUser();
        return this.name;
    }
    public String getPassword(){
        this.readUser();
        return this.password;
    }
    public String getCondition(){
        this.readCondition();
        return this.lastCondition;
    }
    public ArrayList<DataObj.Music> getMusic(){
        this.readMusic();
        return this.music;
    }
    public ArrayList<DataObj.Podcast> getPodcast(){
        this.readPodcast();
        return this.podcast;
    }
    public ArrayList<DataObj.History> getHistory(){
        this.readHistory();
        return this.history;
    }


    /**
     * Method untuk menulis data ke file dat.
     */
    private void writeUser(){
        try {
            FileOutputStream fileOut = new FileOutputStream("user.dat");
            DataOutputStream dataOut = new DataOutputStream(fileOut);

            dataOut.writeUTF(this.username);
            dataOut.writeUTF(this.name);
            dataOut.writeUTF(this.password);

            dataOut.flush();
            dataOut.close();
            fileOut.close();

        } catch (Exception e) {
            return;
        }
    }

    /**
     * Method untuk membaca data dari file dat. 
     */
    private void readUser(){
        try {
            File file = new File("user.dat");
            if (!file.exists() || file.length() == 0){
                return;
            }
            FileInputStream fileIn = new FileInputStream("user.dat");
            DataInputStream dataIn = new DataInputStream(fileIn);

            this.username = dataIn.readUTF();
            this.name = dataIn.readUTF();
            this.password = dataIn.readUTF();

            dataIn.close();
            fileIn.close();
        } catch (Exception e) {
            return;
        }
    }

    /**
     * Method untuk menulis data ke file dat.
     */
    private void writeCondition(){
        try {
            FileOutputStream fileOut = new FileOutputStream("condition.dat");
            DataOutputStream dataOut = new DataOutputStream(fileOut);

            dataOut.writeUTF(this.lastCondition);

            dataOut.flush();
            dataOut.close();
            fileOut.close();
            
        } catch (Exception e) {
            return;
        }
    }

    /**
     * Method untuk membaca data dari file dat. 
     */
    private void readCondition(){
        try {
            File file = new File("condition.dat");
            if (!file.exists() || file.length() == 0){
                return;
            }
            FileInputStream fileIn = new FileInputStream("condition.dat");
            DataInputStream dataIn = new DataInputStream(fileIn);

            this.lastCondition = dataIn.readUTF();

            dataIn.close();
            fileIn.close();
        } catch (Exception e) {
            return;
        }
    }

    /**
     * Method untuk menulis data ke file dat.
     */
    private void writeMusic(){
        try {
            FileOutputStream fileOut = new FileOutputStream("music.dat");
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);

            objectOut.writeObject(this.music);

            objectOut.flush();
            objectOut.close();
            fileOut.close();
        } catch (Exception e) {
            return;
        }
    }

    /**
     * Method untuk membaca data dari file dat. 
     */
    private void readMusic(){
        try {
            File file = new File("music.dat");
            if (!file.exists() || file.length() == 0) {
                return;
            }
            FileInputStream fileIn = new FileInputStream(("music.dat"));
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);

            this.music = (ArrayList<DataObj.Music>) objectIn.readObject();

            objectIn.close();
            fileIn.close();
        } catch (Exception e) {
            return;
        }
    }

    /**
     * Method untuk menulis data ke file dat.
     */
    private void writePodcast(){
        try {
            FileOutputStream fileOut = new FileOutputStream("podcast.dat");
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);

            objectOut.writeObject(this.podcast);

            objectOut.flush();
            objectOut.close();
            fileOut.close();
        } catch (Exception e) {
            return;
        }
    }

    /**
     * Method untuk membaca data dari file dat. 
     */
    private void readPodcast(){
        try {
            File file = new File("podcast.dat");
            if (!file.exists() || file.length() == 0) {
                return;
            }
            FileInputStream fileIn = new FileInputStream(("podcast.dat"));
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);

            this.podcast = (ArrayList<DataObj.Podcast>) objectIn.readObject();

            objectIn.close();
            fileIn.close();
        } catch (Exception e) {
            return;
        }
    }

    /**
     * Method untuk menulis data ke file dat.
     */
    private void writeHistory(){
        try {
            FileOutputStream fileOut = new FileOutputStream("history.dat");
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);

            objectOut.writeObject(this.history);

            objectOut.flush();
            objectOut.close();
            fileOut.close();
        } catch (Exception e) {
            return;
        }
    }
    
    /**
     * Method untuk membaca data dari file dat. 
     */
    private void readHistory(){
        try {
            File file = new File("history.dat");
            if (!file.exists() || file.length() == 0) {
                return;
            }
            FileInputStream fileIn = new FileInputStream(("history.dat"));
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);

            this.history = (ArrayList<DataObj.History>) objectIn.readObject();

            objectIn.close();
            fileIn.close();
        } catch (Exception e) {
            return;
        }
    }

    /**
     * Method untuk menambahkan data konten dari system
     * (data dummy) namun data konten yang lama akan terhapus,
     * agar tidak terjadi duplikasi data. 
     */
    public void updateContent(){
        additional.textToCenter("existing content data will be deleted,");
        additional.textToCenter("are you sure?");
        while(true){
        System.out.print("> (yes/no): ");
        String inputUser = scan.nextLine();
            if (inputUser.equals("yes")) {
                this.music.clear();
                this.writeMusic();
                this.setMusic("Tak Ingin Usai", "Keisya Levronka", "pop", "unavailable");
                this.setMusic("Rumah Singgah", "Fabio Aisher", "pop", "unavailable");
                this.setMusic("Pilihan Terbaik", "Ziva Magnolya", "pop", "unavailable");
                this.setMusic("Diri", "Tulus", "pop", "unavailable");
                this.setMusic("Dunia Tipu-Tipu", "Yura Yunita", "pop", "unavailable");
                this.setMusic("Hingga Tua Bersama", "Rizky Febian", "pop", "unavailable");
                this.setMusic("Sang Dewi", "Lyodra", "pop", "unavailable");
                this.setMusic("As It Was", "Harry Styles", "pop", "unavailable");
                this.setMusic("Left and Right", "Charlie Puth ft Jung Kook", "pop", "unavailable");
                this.setMusic("Anti Hero", "Taylor Swift", "pop", "unavailable");
        
                this.podcast.clear();
                this.writePodcast();
                this.setPodcast("Sudut Pandang", "Aria Notharia", "Edukasi", 20);
                this.setPodcast("Corbuzier", "Deddy Corbuzier", "Talkshow", 15);
                this.setPodcast("Satu Persen", "unavailable", "Psikologi", 25);
                this.setPodcast("Makna Talks", "unavailable", "Talkshow", 37);
                this.setPodcast("Bung2Bung", "Rana dan Rindra", "Berita", 23);
                this.setPodcast("Subjective", "Iqbal Hariadi", "Edukasi", 19);
                this.setPodcast("Muda Cuma Sekali", "Obut", "Edukasi", 32);
                additional.textToCenter("content added successfully");
                break;
            } else if (inputUser.equals("no")) {
                additional.textToCenter("content update canceled");
                break;
            } else {
                additional.inputMismatch();
            }
        }
    }
}
