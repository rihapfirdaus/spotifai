package spotifai;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Class tambahan untuk layout dan tampilan. 
 */
public class Additional {
    
    /**
     * Method untuk membersihkan layar konsol. 
     */
    public void clearScreen(){ 
        Display display = new Display();
        try {
            if (System.getProperty("os.name").contains("Windows")) {
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            } else {
                System.out.print("\033[H\033[2J");
                System.out.flush();
            }
        } catch (Exception ex) {
            System.out.println("Failed to clear screen: " + ex.getMessage());
        }
        display.headerSection();
    }

    /**
     * Method untuk menampilkan salam/sapaan terhadap user. 
     * Sapaan ditampilkan berdasarkan waktu sistem. 
     */
    public void getGreetings(){
        Data data = new Data();
        Calendar now = Calendar.getInstance();

        String nama = data.getName();
        int hour = now.get(Calendar.HOUR_OF_DAY);

        if (nama == null){
            if (hour >= 0 && hour < 12) {
                System.out.println("Good Morning..");
            } else if (hour >= 12 && hour < 15) {
                System.out.println("Good Afternoon..");
            } else if (hour >= 15 && hour < 18) {
                System.out.println("Good Evening..");
            } else {
                System.out.println("Good Night..");
            }
        } else{
            if (hour >= 0 && hour < 12) {
                System.out.println("Good Morning, " + nama + "..");
            } else if (hour >= 12 && hour < 15) {
                System.out.println("Good Afternoon, " + nama + "..");
            } else if (hour >= 15 && hour < 18) {
                System.out.println("Good Evening, " + nama + "..");
            } else {
                System.out.println("Good Night, " + nama + "..");
            }
        }
    }

    /**
     * Method untuk menampilkan string sebagai pemisah antar section.
     */
    public void separator(){
        System.out.println("=".repeat(54));
    }

    /**
     * Method untuk menampilkan string dalam format rata tengah. 
     * @param str String yang akan diberi format
     */
    public void textToCenter(String str){
        int count = str.length();
        int x = (54-count)/2;
        String spaces = " ".repeat(x);
        System.out.println(spaces + str);
    }

    /**
     * Method untuk menampilkan pesan error pada input pengguna
     * yang tidak sesuai dengan yang diminta. 
     */
    public void inputMismatch(){
        this.textToCenter("input does not match");
        this.textToCenter("please reenter");
        this.delay(2000);
        System.out.print("\033[3A\033[J"); 
    }

    /**
     * Method untuk menjeda program selama beberapa saat. 
     * @param times lama waktu menjeda dalam miliseconds
     */
    public void delay(int times){
        try {
            Thread.sleep(times);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method untuk membersihkan layar konsol dari baris
     * sebelum cursor ke-n. 
     * @param delay lama waktu sebelum konsol dibersihkan (miliseconds)
     * @param line jumlah baris yang ingin dibersihkan
     */
    public void clearFromLine(int delay, int line){
        this.delay(delay);
        System.out.print("\033["+ line +"A\033[J"); 
    }

    /**
     * Method untuk menampilkan pesan-pesan pada proses signin/signup
     * sebelum masuk ke menu utama. 
     */
    public void message(){
        ArrayList<String> note = new ArrayList<String>();
        note.add("you must remember your account..");
        note.add("you can change your account info on the profile page");
        note.add("you can listen to your favorite songs here");
        note.add("many podcasts available for free here");
        note.add("lots of songs waiting to be listened to");
        note.add("anywhere and anytime you want");
        
        for (int i = 0; i < note.size(); i++) {
            this.textToCenter(note.get(i));
            this.textToCenter("please wait..");
            if (i < note.size()) {
                this.delay(2500);
                System.out.print("\033[2A\033[J"); 
            } else {
                this.delay(2000);
            }
        }
    }

    /**
     * Method untuk menampilkan pesan saat user keluar dari program 
     * dan mengubah kondisi player menjadi pause. 
     */
    public void exitMessage(){
        Data data = new Data();
        this.separator();
        this.textToCenter("Thank you for using spotifai");
        data.setCondition(2);
    }
}
